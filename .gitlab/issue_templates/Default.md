# Titre de la tâche/issue

## Objectifs

Courte description des objectifs de la tâche/issue

## Asset

## Informations

Toutes informations pertinentes pour faciliter la réalisation de la tâche/issue

## Number of vulnerabilities - Initial

## Initial risk level

## Action plan owner

## XXXXX SEC follow-up

## Status/Comments

## Description périmètre et limitation

## Commentaires confidentiels

## Description détaillée

Description détaillée, spécifications ou expression de besoin.
